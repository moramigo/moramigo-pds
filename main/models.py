from django.contrib.auth.models import User
from django.db import models
import datetime
class Pessoa(models.Model):
    
    GENERO_CHOICES = (
        ('M', "Masculino"),
        ('F', "Feminino"),
        ('O', "Outra classificação")
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(max_length=255)
    senha = models.CharField(max_length=30)
    # foto = models.ImageField()
    descricao = models.CharField(max_length=255, blank=True)
    nome = models.CharField(max_length=255)
    data_nascimento = models.DateTimeField("Data de nascimento")
    genero = models.CharField(max_length=1, choices=GENERO_CHOICES)
    ocupacao = models.CharField(max_length=40)
    contato = models.CharField(max_length=10)
    interessados = models.ManyToManyField("Pessoa", blank=True, symmetrical=False, related_name='interesses')
 
    def __str__(self):
        return self.nome

    def idade(self):
        return datetime.datetime.today().year - self.data_nascimento.year

class Restricoes(models.Model):    
    pessoa = models.OneToOneField(Pessoa, on_delete=models.CASCADE)    
    
    PREFERENCIA_GENERO_CHOICES = (
        ('M', "Quero morar apenas com homens."),
        ('F', "Quero morar apenas com mulheres."),
        ('I', "Não tenho preferência do gênero com quem quero dividir.")
    )
    ANIMAIS_CHOICES = (
        ('S', "Moraria numa casa com animais."),
        ('D', "Moraria dependendo do animal."),
        ('N', "Não moraria com animais."),
        ('I', "Tanto faz para mim morar ou não com animais.")
    )
    VISITAS_CHOICES = (
        ('S', "Posso morar tranquilamente com pessoas que recebem visitas."),
        ('N', "Não quero mesmo morar com pessoas que recebem visitas."),
        ('I', "Tanto faz para mim morar ou não com pessoas que recebem visitas."),        
    )
    BEBIDAS_CHOICES = (
        ('S', "Posso morar tranquilamente com pessoas que bebem."),
        ('N', "Não quero mesmo morar com pessoas que bebem."),
        ('I', "Tanto faz pra mim morar ou não com pessoas que bebem."),
    )
    FUMANTES_CHOICES = (
        ('S', "Posso morar tranquilamente com pessoas que fumam."),
        ('N', "Não quero mesmo morar com pessoas que fumam."),
        ('I', "Tanto faz pra mim morar ou não com pessoas que fumam."),        
    )
    valor_contribuicao = models.DecimalField(max_digits=8, decimal_places=2)    
    genero_companheiro = models.CharField(max_length=1, choices=PREFERENCIA_GENERO_CHOICES)
    animais = models.CharField(max_length=1, choices=ANIMAIS_CHOICES)
    visitas = models.CharField(max_length=1, choices=VISITAS_CHOICES)
    bebidas = models.CharField(max_length=1, choices=BEBIDAS_CHOICES)
    fumantes = models.CharField(max_length=1, choices=FUMANTES_CHOICES)
    localidades = models.ManyToManyField("Bairro", related_name='restricoes')

    def __str__(self):
        return "Restrição de {}".format(self.pessoa)

class Bairro(models.Model):
    nome = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.nome
