from django.contrib import admin
from .models import Restricoes, Pessoa, Bairro

class RestricoesInline(admin.TabularInline):
    model = Restricoes

class PessoaAdmin(admin.ModelAdmin):
    inlines = [RestricoesInline]

admin.site.register(Pessoa, PessoaAdmin)
admin.site.register(Bairro)