from .models import Restricoes
import django_filters

class RestricoesFilter(django_filters.FilterSet):

    class Meta:
        model = Restricoes
        fields = [
            # 'valor_contribuicao',
            'genero_companheiro',
            'animais',
            'visitas',
            'bebidas',
            'fumantes',
            'localidades'
        ]
