from django.urls import path
from . import views


app_name = 'main'
urlpatterns = [
    path('', views.HomeView.as_view(), name="home"),
    #path('busca', views.BuscaView.as_view(), name="busca"),
    path('perfil/<int:pk>', views.PerfilView.as_view(), name="perfil"),
    path('login', views.login_view, name="login"),
    path('logout', views.logout_view, name="logout"),
    path('cadastro-usuario', views.CadastroView.as_view(), name="cadastro-usuario"),
    path('buscar-pessoas', views.BuscarPessoas, name='buscar-pessoas'),

    #path('interesse/<int:pk>', views.DemonstrarInteresseView, name="interesse"),
    #path('interessados/<int:pk>', views.interessados, name='interessados'),
    
    path('demonstrar-interesse/<int:pk>', views.DemonstrarInteresse, name='demonstrar-interesse'),    
    path('meus-interesses/<int:pk>', views.MeusInteresses, name='meus-interesses'),
    path('solicitacoes-interesse/<int:pk>', views.SolicitacoesInteresse, name='solicitacoes-interesse'),
    path('aceitar-interesse/<int:pk>', views.AceitarInteresse, name='aceitar'),
    path('recusar-interesse/<int:pk>', views.RecusarInteresse, name='recusar'),


]