from django.shortcuts import render, reverse, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views import generic
from  .models import Pessoa, Restricoes
from .filters import RestricoesFilter

class HomeView(generic.ListView):
    model = Pessoa
    template_name = "main/home.html"

def BuscarPessoas(request):
    restricoes = Restricoes.objects.all()
    restricoes_filter = RestricoesFilter(request.GET, queryset=restricoes)
    return render(request, 'main/buscar-pessoas.html', {'restricoes':restricoes_filter})

class BuscaView(generic.ListView):
    model = Pessoa
    template_name = 'main/busca.html'

class PerfilView(generic.DetailView):
    model = Pessoa
    template_name = 'main/perfil.html'

def login_view(request):
    if request.user.is_authenticated:
        return redirect(reverse('main:home'))
    else:
        if request.method == 'POST':
            usuario = request.POST.get('username')
            senha = request.POST.get('senha')
            tentativa_login = authenticate(request, username=usuario, password=senha)

            if tentativa_login is not None:
                login(request, tentativa_login)
                return redirect(reverse('main:home'))
            else:
                # messages.error(request, 'Usuário não encontrado: tente novamente')
                messages.error(request, 'O nome de usuário inserido ou senha está Incorreto. Verifique  e tente novamente.')


        return render(request, 'main/login.html', {})

def logout_view(request):
    logout(request)
    return redirect(reverse('main:home'))

class CadastroView(generic.TemplateView):
    model = Pessoa
    template_name = 'main/cadastro-usuario.html'

@login_required(login_url='main:login')
def DemonstrarInteresse(request, pk):
    pessoa_que_vou_demonstrar_interesse = Pessoa.objects.get(pk=pk)
    if (pessoa_que_vou_demonstrar_interesse.id != request.user.pessoa.id):
        request.user.pessoa.interessados.add(pessoa_que_vou_demonstrar_interesse)
    else:
        print("Não posso demonstrar interesse no meu próprio perfil")
        return redirect(reverse('main:perfil', args=(request.user.pessoa.id,)))
    return render(request, 'main/interesse.html', {})

@login_required(login_url='main:login')
def MeusInteresses(request, pk):
    meus_interesses = request.user.pessoa.interessados.all()
    solicitacoes_aceitas = []
    solicitacoes_pedentes = []
    for solicitacao in meus_interesses:
        if request.user.pessoa in solicitacao.interessados.all(): #Se o usuário estiver nos interessados da outra pessoa
            solicitacoes_aceitas.append(solicitacao)
        else:
            solicitacoes_pedentes.append(solicitacao)

    return render(request, 'main/interesse.html', {'solicitacoes_aceitas':solicitacoes_aceitas, 'solicitacoes_pedentes':solicitacoes_pedentes})

@login_required(login_url='main:login')
def SolicitacoesInteresse(request, pk):
    meus_interesses = request.user.pessoa.interessados.all() #Quem já aceitei
    solicitacoes_interesse = request.user.pessoa.interesses.all() #Quem já aceitei e quem não aceitei
    nao_aceito = [p for p in solicitacoes_interesse if p not in meus_interesses] #Filtrar as solicitações de interesse
    print(nao_aceito)
    return render(request, 'main/meus-interesses.html', {'solicitacoes':nao_aceito})

@login_required(login_url='main:login')
def AceitarInteresse(request, pk):
    pessoa_aceita = Pessoa.objects.get(pk=pk)
    request.user.pessoa.interessados.add(pessoa_aceita)
    #request.user.pessoa.interesses.delete(pk=request.user.pessoa.id)
    return render(request, 'main/meus-interesses.html', {})

@login_required(login_url='main:login')
def RecusarInteresse(request, pk):
    pessoa_recusada = Pessoa.objects.get(pk=pk) #Recuperando id, pessoa recusada
    request.user.pessoa.interesses.remove(pessoa_recusada) #Removendo das solicitações de interesse a pessoa recusada
    return redirect(reverse('main:solicitacoes-interesse', args=(request.user.pessoa.id,)))